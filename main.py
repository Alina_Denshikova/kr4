import numpy as np
import matplotlib.pyplot as plt
import timeit

A = np.random.randint(0, 100, size=(500, 200))
B = np.random.randint(0, 100, size=(500, 200))
starttime = timeit.default_timer()
print("начальное время:", starttime)
print(np.multiply(A, B))
print("затраченное время:", timeit.default_timer() - starttime)

plt.plot([1000, 10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000],
         [0.006208199999999997, 0.00021219999999999573, 0.0002314999999999401, 0.0002657999999999827,
          0.0002573999999999632, 0.00028640000000001997, 0.0004969999999999697, 0.0004612999999999978,
          0.00033640000000001447, 0.0004135000000000666, 0.0004145999999999317],
         linewidth=2.0)
plt.xlabel('размерность матрицы')
plt.ylabel('время')
plt.show()
